<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Pet;

class PetController extends AbstractController
{
    /**
     * @Route("/pet", name="pet")
     */
    public function new(Request $request)
    {

        $pet = new Pet();

        $form = $this->createFormBuilder($pet)
        ->add('name', TextType::class)
        ->add('age', NumberType::class)
        ->add('breed', TextType::class)
        ->add('serialNumber', NumberType::class)
        ->add('save', SubmitType::class, ['label' => 'Save the pet'])
        ->getForm();

        $form->handleRequest($request);
    

        if ($form->isSubmitted()) {
            $pet = $form->getData();
           
            $enitityManager = $this->getDoctrine()->getManager();
            $enitityManager->persist($pet);
            $enitityManager->flush();
        } 
        return $this->render('pet.html.twig', [
            'form' => $form->createView(),
        ]); 
    }
}
